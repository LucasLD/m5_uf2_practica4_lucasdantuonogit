package calculadora;

import java.util.Scanner;



//COMENTARI NOU: enfocat a la 'pràctica 4' -> he fet aquest comentari para provoca una notificació de modificació d'aquesta clase en Git.
// Aquesta clase esta comentada de forma correcta amb JavaDoc. 
//
//// UN ALTRA COMENTARI NOU: Aquest comentari es especial, després de fer merge master amb branca1, ara estic fent una modificació
// des de GitLab! Aquest commit es fará des de aqui! (posicionat branca master quan he escrit aquet comentari)
// Els comentaris de GitLab i la branca master que he fet merge, entraran en conflicte ja que he modificat el mateix arxiu des de
// diferents plataformas, Quin és el bó? El més actualitzat!


/**
 * <h2> Info: Clase 'LaCalculadora' </h2>
 * 
 * Busca información de Javadoc en <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 1.0-2023
 * @author Lucas D'Antuono
 * @since 18-2-2023
 */
public class LaCalculadora {

	
	/**Clase importada 'Scanner'. Servirà per llegir i guardar els valors introduïts per l'usuari quan sigui necesari.
	 * 
	 *
	 * 
	 * 
	 *@see java.util.Scanner
	 * 
	 */
    static Scanner reader = new Scanner(System.in);
    
    
    
    /** Clase main() o principal:
     * <br>
     * Clase principal que executara un codi que farà la crida de les funcions i mètodes definits.
     * 
     * 
     *
     * 
     */
	public static void main(String[] args) {
		
		int op1 = 0, op2 = 0;
		char opcio;
		int resultat = 0;

		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-':
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*':
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/':
                    if (control) {
                        resultat = divisio(op1, op2);
                        if (resultat == -99)
                            System.out.print("No ha fet la divisió");
                        else
                             visualitzar(resultat);
					}
		            else mostrarError();
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opció erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}
	
	
	
	/**
	 * Aquest procediment mostrarà un error en pantalla afirmant que cal introduïr els valors a operar.
	 * Aquest procedient es llançarà en el menú si seleccionem una operació sense haver introduït abans els valors a operar.
	 * 
	 * @see LaCalculadora
	 *
	 */
    public static void mostrarError() { /* procediment */
		System.out.println("\nError, cal introduir primer els valors a operar");
	}


    
    

    /**
	 * Aquesta funció farà la suma dels valors 'a' i 'b' que rep com paràmetre d'entrada. Retornarà el valor del resultat d'aquesta operació (variable 'res').
	 * 
	 * @param a Valor del primer enter que rep la funció. Variable tipo 'int'.
	 * @param b Valor del segon enter a operar que rep la funció. Variable tipo 'int'.
	 * 
	 * @return <ul>
	 *                   <li>'res': aquesta variable emmagatzena el valor del resultat de la operació.</li>                 
	 *              </ul>
	 */
	public static int suma(int a, int b) { /* funció */
		int res;
		res = a + b;
		return res;
	}
	
	
	
	
	
    /**
	 * Aquesta funció farà la resta dels valors enters 'a' i 'b' que  rep com paràmetre d'entrada. Retornarà el valor del resultat d'aquesta operació.
	 * 
	 *@param a Valor del primer enter que rep la funció com paràmetre d'entrada. Variable tipo 'int'.
	 *@param b Valor del segon enter a operar que rep la funció com paràmetre d'entrada. Variable tipo 'int'.
	 *
	 *@return <ul>
	 *                   <li>'res': aquesta variable emmagatzena el valor del resultat de la operació.</li>                 
	 *              </ul>
	 */
	public static int resta(int a, int b) { /* funció */
		int res;
		res = a - b;
		return res;
	}
	
	
	
	/**
	 * Funció: 'Multiplicació' -> Aquesta operació fara la multiplicació dels valors enters 'a' i 'b'.
	 * 
	 * 
	 *@param a Valor del primer enter que rep la funció com paràmetre d'entrada. Variable tipo 'int'.
	 *@param b Valor del segon enter a operar que rep la funció com paràmetre d'entrada. Variable tipo 'int'.
	 * 
	 *@return <ul>
	 *                   <li>'res': aquesta variable emmagatzena el valor de la operació. Variable tipo 'int'.</li>                 
	 *              </ul>
	 */
	public static int multiplicacio(int a, int b) { /* funció */
		int res;
		res = a * b;
		return res;
	}
	
	
	
	/**
	 * Funció: 'Divisio' -> Aquesta funció fara la divisió dels paràmetres rebuts 'a' i 'b'. La funció s'encarrega de retornar el resutlat
	 * 						de la divisió o del módul, segons indiqui l'usuari amb les lletres 'M'/'m' (módul) o 'D'/'d'(divisió).
	 * 						El codi de la funció està estructurat dins un do-while que s'encarrega primerament de la decisió del usuari (módul o divisió).
	 * 						Si el usuari no introdueix un caractèr correcte, 'while' s'encarrega de tornar a demanar.
	 * 						Posteriorment, en una estructura if-else es comproba que el segón valor enter ('b') no sigui 0.
	 * 
	 * @param a Valor del primer enter que rep la funció com paràmetre d'entrada. Variable tipo 'int'.
	 * @param b Valor del segon enter a operar que rep la funció com paràmetre d'entrada. Variable tipo 'int'.
	 * 
	 * 
	 * @return <ul>
	 *                   <li>'res': aquesta variable emmagatzena el valor del resultat de la operació entre els valors 'a' i 'b'.</li>                 
	 *              </ul>
	 */
	public static int divisio(int a, int b) { /* funció */
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opció incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}
	
	
	/**
	 * Aquesta funció s'encarrega de guardar un caràcter que introduïra el usuari desprès de mostrar un
	 * missatge en pantalla 'Intrdoueix un carácter:'. Finalment, retorna el caràcter.
	 * 	
	 *@see java.util.Scanner 			
	 *		
	 * @return <ul>
	 *                   <li>'car': Aquesta variable guarda el caràcter introduït per l'usuari. Variable tipo 'char'.</li>                 
	 *              </ul>
	 */
	public static char llegirCar() // funció
	{
		char car;

		System.out.print("Introdueix un carácter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}
	
	
	/**
	 *Aquesta funció s'encarrega de guardar un enter que introduïra el usuari desprès de mostrar un
	 * missatge en pantalla 'Intrdoueix un valor enter:' i finalment  lo retornarà
	 * 
	 * Dins una estrucutra 'try-catch'  en bucle do-while es demanarà per pantalla introduïr un valor enter. Posteriorment, amb un Scanner s'agafara el valor.
	 * El valor serà valid si és enter. En cas contrari, es llençarà un error que es mostrara en pantalla i tornarà a demanar el valor enter.
	 * 
	 * @see java.util.Scanner
	 * 						
	 * 
	 * 
	 * @return <ul>
	 *                   <li>'valor': Aquesta variable emmagatzena el enter introduït per l'usuari. Variable tipo 'int'.</li>                 
	 *              </ul>
	 *              
	 *@exception  e  Es mostrara 'Exception' -  "Error, s'espera un valor enter"
	 *
	 */
	public static int llegirEnter() // funció
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}
	
	
	/**
	 *Aquesta funció mostrara en pantalla un valor enter que rep com paràmetre d'entrada.
	 *Aquesta fucnió esta enfocada en mostrar resultats que rep com paràmetre.
	 * 						
	 * @param res Guarda el resultat que posteiorment es mostrara en pantalla. Variable tipo 'int'.
	 * 
	 * 
	 */
	public static void visualitzar(int res) { /* procediment */
		System.out.println("\nEl resultat de l'operacio és " + res);
	}

	/**
	 * La funció 'mostrar_menu'; quan es invocada mostrara en pantalla un menu de opcions dirigit al usuari.
	 * 
	 * 
	 *@see LaCalculadora
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opció i ");
	}

}
